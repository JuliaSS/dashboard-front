export const state = () => ({
    TimeNumberCalls: [],

    receivedCalls: [],
    receivedAppeal: [],
    closeAppeal: [],
    
    incomingCall: [],
    avgCallTimes: [],

    analysisReceivedRequests: [],
    analysisReceivedCalls: [],
    analysisCloseCalls: [],

    statisticsUrl: [
        'number-of-calls-received',// pie
        'number-of-applications-received',
        'number-of-applications-close-received',
        'count-calls',//line 3
        'analysis-received-requests',//bar 4
        'analysis-received-calls',//bar 5
        'analysis-close-received-calls',// one bar
        'avg-call-times',// 7 среднее продолжительность звонков в разрезе тг
    ],
  })
  export const mutations = {
    SET_TIME_NUMBER_CALLS(state, statistics) {
      state.TimeNumberCalls = statistics
    },
    SET_RECEIVED_CALLS(state, statistics) {
        state.receivedCalls = statistics
    },
    SET_RECEIVED_APPEAL(state, statistics) {
        state.receivedAppeal = statistics
    },
    SET_CLOSE_APPEAL(state, statistics) {
        state.closeAppeal = statistics
    },


    SET_INCOMING_CALLS(state, statistics) {
        state.incomingCall = statistics
    },
    SET_AVG_CALLS_TIME(state, statistics) {
        state.avgCallTimes = statistics
    },

    
    SET_ANALYSIS_RECEIVED_REQUESTS(state, statistics) {
        state.analysisReceivedRequests = statistics
    },
    SET_ANALYSIS_RECEIVED_CALLS(state, statistics) {
        state.analysisReceivedCalls = statistics
    },
    SET_ANALYSIS_CLOSE_CALLS(state, statistics) {
        state.analysisCloseCalls = statistics
    },
    
  }
  import axios from 'axios';
  export const actions = {
    async fetch({commit}, url) {
      const statisticsList = await axios.get(`https://xn--c1aj1aj.xn--p1ai/dashboard-service/api/v1/mdlp/${url}`)
        switch (url) {
            case 'number-of-calls-received':
            commit('SET_RECEIVED_CALLS', statisticsList.data.data)
            break;
            case 'number-of-applications-received':
            commit('SET_RECEIVED_APPEAL', statisticsList.data.data)
            break;
            case 'number-of-applications-close-received':
            commit('SET_CLOSE_APPEAL', statisticsList.data.data)
            break;

            case 'count-calls':
            commit('SET_INCOMING_CALLS', statisticsList.data.data)
            break;
            case 'avg-call-times':
            commit('SET_AVG_CALLS_TIME', statisticsList.data.data)
            break;

            case 'analysis-received-requests':
            commit('SET_ANALYSIS_RECEIVED_REQUESTS', statisticsList.data.data)
            break;
            case 'analysis-received-calls':
            commit('SET_ANALYSIS_RECEIVED_CALLS', statisticsList.data.data)
            break;
            case 'analysis-close-received-calls':
            commit('SET_ANALYSIS_CLOSE_CALLS', statisticsList.data.data)
            break;
                
            default:
            break;
        }
    },
    async fetchCall({commit},url){
          const statisticsList = await axios.get(`https://xn--c1aj1aj.xn--p1ai/dashboard-service/api/v1/mdlp/average-call-time/${url}`)
          commit('SET_TIME_NUMBER_CALLS', statisticsList.data.data)
    },
  }
  export const getters = {
    TIME_NUMBER_CALLS: s => s.TimeNumberCalls,
    RECEIVED_CALLS: s=> s.receivedCalls,
    RECEIVED_APPEAL: s=> s.receivedAppeal,
    CLOSE_APPEAL: s=> s.closeAppeal,

    INCOMING_CALLS: s=> s.incomingCall,
    AVG_CALLS_TIME: s=> s.avgCallTimes,

    ANALYSIS_RECEIVED_REQUESTS: s=> s.analysisReceivedRequests,
    ANALYSIS_RECEIVED_CALLS: s=> s.analysisReceivedCalls,
    ANALYSIS_CLOSE_CALLS: s=> s.analysisCloseCalls,

    STATISTICS_URL:s=>s.statisticsUrl,
  }